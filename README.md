# libvoxl_io

Userspace library for interfacing with VOXL IO ports via the Sensors DSP (sdsp). Currently supports UART, but I2C, SPI, and GPIO functions are in the works.

For more documentation on how to use, visit:
https://docs.modalai.com/voxl-io-guides/

## Build Instructions

1) Requires the voxl-hexagon docker image to build. Please follow the voxl-docker instructions:
https://gitlab.com/voxl-public/voxl-docker

2) Pull in the mavlink git submodule

```bash
git submodule update --init --recursive
```

3) Launch the voxl-hexagon docker image in privaledged mode in the libvoxl-io source directory.

Note, the -p option in voxl-docker was added May 29, 2020. You may need to update voxl-docker.

```bash
~/git/libvoxl_io$ voxl-docker -i voxl-hexagon -p
user@57f6e83bba92:~$
```

4) Run build.sh inside the docker.

```bash
user@57f6e83bba92:~$ ./build.sh
```

## Installation

4) Generate an ipk package inside the docker.

```bash
user@57f6e83bba92:~$ ./make_package.sh
```

5) You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/libvoxl_io$ ./install_on_voxl.sh
```
